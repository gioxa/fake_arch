# fake_arch


Fake_arch is a LD_PRELOAD lib to return the environment variable: `FAKE_ARCH` if set for `uname()`


## example:

```bash
$> uname -m
x86_64

$> EXPORT FAKE_ARCH=test_arch

$> LD_PRELOAD=libfakearch.so uname -m
test_arch

$> unset FAKE_ARCH

$> LD_PRELOAD=libfakearch.so uname -m
x86_64
```

## Purpose

rpm install a foreign architecture in a new rootfs

yum download a foreign architecture


# install

add deployctl repository as:

```bash
curl https://repo.deployctl.com/repo.rpm.sh | sudo bash
```

Install:

```bash
yum install fake_arch
```

