/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
 /*! @file fake_arch.c
  *  @brief fake an architecture
	Setting environment variable *FAKE_ARCH* in combination with an *LD_PRELOAD* will return from uname() the FAKE_ARCH if set.
	
	usage:
	@code
	export FAKE_ARCH=armv7hl
	LD_PRELOAD=fake_arch.so uname
	@endcode
  *  @author danny@gioxa.com
  *  @date 15/10/17
  *  @copyright (c) 2018 Danny Goossen,Gioxa Ltd.
  */
#define _GNU_SOURCE
#include "../config.h"
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/utsname.h>
#include <string.h>

static int(*uname_internal)(struct utsname * unameData);

void __attribute__ ((constructor)) init(void);

/* Library constructor */
void
init(void)
{
	uname_internal = dlsym(RTLD_NEXT, "uname");
}

int uname(struct utsname * unameData)
{
	int ret;

	ret = uname_internal(unameData);
	//sprintf(unameData->machine,"%s","armv7hl");
	char * fake_arch=getenv("FAKE_ARCH");
	if (fake_arch)
	  sprintf(unameData->machine,"%s",fake_arch);
	return ret;
}

