#!/bin/bash
set -e
echo "LD_PRELOAD LIB to TEST: $ld_lib"
ORG_ARCH="$(uname -m | tr -d '\n')"
echo "No Preload uname -m=$ORG_ARCH"
export FAKE_ARCH=funny
result="$(LD_PRELOAD=$ld_lib uname -m | tr -d '\n')"
echo "Preload FAKE_ARCH=$FAKE_ARCH: uname -m=$result"
test "$result" == "$FAKE_ARCH"
unset FAKE_ARCH
result="$(LD_PRELOAD=$ld_lib uname -m | tr -d '\n')"
echo "Preload unset FAKE_ARCH: uname -m=$result"
test "$result" == "$ORG_ARCH"
